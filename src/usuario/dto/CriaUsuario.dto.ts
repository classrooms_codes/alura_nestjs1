import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';
import { ValidateEmail } from '../validations/SingleMail.validator';

export class CriaUsuarioDto {
  @IsNotEmpty({
    message: 'O nome não pode estar vazio',
  })
  name: string;

  @IsEmail(undefined, { message: 'Informe um e-mail válido' })
  @IsNotEmpty({
    message: 'O email não pode estar vazio',
  })
  @ValidateEmail({ message: 'Já existe um usuário com este e-mail' })
  email: string;

  @IsNotEmpty({
    message: 'A senha não pode estar vazia',
  })
  @MinLength(6, { message: 'A senha precisa ter, no mínimo, 6 caracteres' })
  password: string;
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { UsuarioEntity } from './usuario.entity';
import { AtualizaUsuarioDTO } from './dto/AtualizaUsuario.dto';

@Injectable()
export class UsuarioRepository {
  private users: UsuarioEntity[] = [];

  async create(user: UsuarioEntity) {
    this.users.push(user);
  }

  async list() {
    if (this.users.length === 0) {
      return {
        message: 'No Data was found!',
      };
    }
    return {
      acknowledge: true,
      size: this.users.length,
      content: this.users,
    };
  }

  async findByEmail(email: string): Promise<boolean> {
    const user = this.users.find((users) => users.email === email);

    return user !== undefined;
  }

  async atualizaUsuario(id: string, data: AtualizaUsuarioDTO) {
    const findData = this.users.findIndex((user) => {
      return user.usuarioId === id;
    });

    this.users[findData] = {
      usuarioId: id,
      name: data.name,
      email: data.email,
      password: data.password,
    };

    return this.users[findData];
  }

  async delete(id: string) {
    const findData = this.users.findIndex((user) => {
      return user.usuarioId === id;
    });

    if (!findData) {
      throw new NotFoundException('Data not found');
    }

    this.users.splice(findData, 1);
    return {
      message: 'User deleted',
    };
  }
}

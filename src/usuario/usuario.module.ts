import { Module } from '@nestjs/common';
import { UsuarioController } from './usuario.controller';
import { UsuarioRepository } from './usuario.repository';
import { SingleMailValidator } from './validations/SingleMail.validator';

@Module({
  controllers: [UsuarioController],
  providers: [UsuarioRepository, SingleMailValidator],
  imports: [],
})
export class UsuarioModule {}

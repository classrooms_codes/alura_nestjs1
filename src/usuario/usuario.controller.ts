import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UsuarioRepository } from './usuario.repository';
import { CriaUsuarioDto } from './dto/CriaUsuario.dto';
import { UsuarioEntity } from './usuario.entity';
import { v4 as uuid } from 'uuid';
import { AtualizaUsuarioDTO } from './dto/AtualizaUsuario.dto';

@Controller({
  version: '1',
  path: '/usuarios',
})
export class UsuarioController {
  constructor(private usuarioRepository: UsuarioRepository) {}

  @Post('/')
  async criaUsuario(@Body() data: CriaUsuarioDto) {
    const usuarioEntity = new UsuarioEntity();
    usuarioEntity.email = data.email;
    usuarioEntity.name = data.name;
    usuarioEntity.password = data.password;
    usuarioEntity.usuarioId = uuid();

    await this.usuarioRepository.create(usuarioEntity);
    return data;
  }

  @Get('/')
  async listaUsuarios() {
    return this.usuarioRepository.list();
  }

  @Put('/atualiza/:id')
  async atualizaUsuario(
    @Param('id') id: string,
    @Body() data: AtualizaUsuarioDTO,
  ) {
    return this.usuarioRepository.atualizaUsuario(id, data);
  }

  @Delete('/deleta/:id')
  async deletaUsuario(@Param('id') id: string) {
    return this.usuarioRepository.delete(id);
  }
}

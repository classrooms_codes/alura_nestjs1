import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UsuarioRepository } from '../usuario.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
@ValidatorConstraint({ async: true })
export class SingleMailValidator implements ValidatorConstraintInterface {
  constructor(private readonly repository: UsuarioRepository) {}

  async validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    const userMailExists = await this.repository.findByEmail(value);
    return !userMailExists;
  }
}

// Função que servirá de decorator para a classe acima
export const ValidateEmail = (validateOptions: ValidationOptions) => {
  return (data: object, property: string) => {
    registerDecorator({
      target: data.constructor,
      propertyName: property,
      options: validateOptions,
      constraints: [],
      validator: SingleMailValidator,
    });
  };
};

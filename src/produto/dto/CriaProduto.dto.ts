import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUrl,
  MaxLength,
  Min,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CaracteristicaProdutoDTO {
  @IsString()
  @IsNotEmpty({ message: 'Informe uma característica' })
  nome: string;

  @IsString()
  @IsNotEmpty({ message: 'Informe uma descrição para a característica' })
  descricao: string;
}

export class ImagemProdutoDTO {
  @IsUrl(undefined, { message: 'Informe uma URL válida' })
  url: string;

  @IsString()
  @IsNotEmpty({ message: 'Informe uma descrição para a imagem' })
  descricao: string;
}

export class CriaProdutoDTO {
  @IsString()
  @IsNotEmpty({ message: 'Informe o nome do produto' })
  nome: string;

  @IsNumber({ maxDecimalPlaces: 2, allowInfinity: false, allowNaN: false })
  @Min(1, { message: 'O valor precisa ser maior que zero' })
  valor: number;

  @IsNumber({ allowInfinity: false, allowNaN: false })
  @Min(0, { message: 'Quantidade mínima inválida' })
  quantidade: number;

  @IsString()
  @IsNotEmpty({ message: 'Informe uma descrição' })
  @MaxLength(1000, {
    message: 'Descrição do produto não pode ter mais que 1000 caracteres',
  })
  descricao: string;

  @ValidateNested()
  @IsArray()
  @ArrayMinSize(1)
  @ArrayMaxSize(3, { message: 'Informe, no máximo, 3 características' })
  @Type(() => CaracteristicaProdutoDTO)
  caracteristicas: CaracteristicaProdutoDTO[];

  @ValidateNested()
  @IsArray()
  @ArrayMinSize(1)
  @ArrayMaxSize(3, { message: 'Informe, no máximo, 3 imagens' })
  @Type(() => ImagemProdutoDTO)
  imagens: ImagemProdutoDTO[];

  @IsString()
  @IsNotEmpty({ message: 'Informe uma categoria' })
  categoria: string;
}

import { Injectable } from '@nestjs/common';

@Injectable()
export class ProdutoRepository {
  private produtos: Array<any> = [];

  async create(dadosProduto) {
    this.produtos.push(dadosProduto);
  }

  async list() {
    if (this.produtos.length === 0) {
      return {
        message: 'No Data was found!',
      };
    }
    return {
      acknowledge: true,
      size: this.produtos.length,
      content: this.produtos,
    };
  }
}

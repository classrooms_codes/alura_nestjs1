import { Body, Controller, Get, Post } from '@nestjs/common';
import { ProdutoRepository } from './produto.repository';
import { CriaProdutoDTO } from './dto/CriaProduto.dto';

@Controller('/produtos')
export class ProdutoController {
  constructor(private repository: ProdutoRepository) {}

  @Post('/')
  async create(@Body() data: CriaProdutoDTO) {
    return this.repository.create(data);
  }

  @Get('/')
  async list() {
    return this.repository.list();
  }
}
